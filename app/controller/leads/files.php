<?php

use app\Models\ZohoCrmClient;
use Symfony\Component\HttpFoundation\JsonResponse;

try {

    $entityId = ! empty($request->get('entityId')) ? $request->get('entityId') : [];
    $record   = new ZohoCrmClient('Leads', getenv("REFRESH_TOKEN_TO_ZOHO_v2"), getenv("ZOHO_USER_INDENTIFIER"));

    $response = $record->getFilesById($entityId);
    if ( ! $response['error']) {
        foreach ($response['data'] as $attchmentIns) {
            $record->downloadFiles($entityId, $attchmentIns->getId());
        }
    }
} catch
(ZCRMException $e) {
    echo $e->getMessage();
    echo $e->getExceptionCode();
    echo $e->getCode();
}
