<?php
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use app\Models\ZohoCrmClient;


$data = !empty($request->request->get('data')) ? $request->request->get('data') : [];
$zohoCrmClient = new ZohoCrmClient('Leads', getenv("REFRESH_TOKEN_TO_ZOHO_v2"), getenv("ZOHO_USER_INDENTIFIER"));
$result = $zohoCrmClient->updateRecord($data);

return new JsonResponse($result);
