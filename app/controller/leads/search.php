<?php
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use app\Models\ZohoCrmClient;


$entityId = !empty($request->get('entityId')) ? $request->get('entityId') : [];
$zohoCrmClient = new ZohoCrmClient('Leads', getenv("REFRESH_TOKEN_TO_ZOHO_v2"), getenv("ZOHO_USER_INDENTIFIER"));
$result = $zohoCrmClient->getRecordById($entityId);

return new JsonResponse($result);
