<?php
ini_set("display_errors", 1);
error_reporting(-1);
use Symfony\Component\HttpFoundation\Response;
use app\Models\ZohoCrmClient;
use Symfony\Component\HttpFoundation\JsonResponse;

try {
    $entityId = !empty($request->get('entityId')) ? $request->get('entityId') : [];
    $file = !empty($_FILES['file_contents']) ? $_FILES['file_contents'] : [];

    $uploaddir = realpath('./') . '/files/'.$entityId.'/';

    $file_information = explode(".", $file['name']);
    
    $filename = substr($file_information[0],0 , 50); 
    $filename.=".".end($file_information);
    
    $uploadFile = $uploaddir.basename($filename);


	if (!file_exists($uploaddir)) {
	    mkdir($uploaddir, 0775, true);
	}    

    if (move_uploaded_file($_FILES['file_contents']['tmp_name'], $uploadFile)) {    
        $record   = new ZohoCrmClient('Leads', getenv("REFRESH_TOKEN_TO_ZOHO_v2"), getenv("ZOHO_USER_INDENTIFIER"));

        $response = $record->uploadAttachment($entityId, $uploadFile);
    
        return new JsonResponse($response);    
    } else {
        $response = [
            "error" => true,
            "message" => "Error to Upload the File".$uploadFile
        ];

        return new JsonResponse($response);    
    }
    


    

} catch (ZCRMException $e) {
    echo $e->getMessage();
    echo $e->getExceptionCode();
    echo $e->getCode();
}
