<?php namespace app\Models;

class ZohoCrmClient 
{
    private $_token;
    private $_userIdentifier;
    private $_refreshToken;
    private $_module;
    private $_zcrmRecord;
    private $_data;


    function __construct($module, $token, $userIdentifier) {
        $this->_token = $token;
        $this->_userIdentifier = $userIdentifier;
        $this->setModule($module);

        $this->_refreshToken = $this->refresToken();
    }

    private function refresToken()
    {

        \ZCRMRestClient::initialize([
            "apiBaseUrl"=>getenv("ZOHO_apiBaseUrl"),
            "apiVersion"=>getenv("ZOHO_apiVersion"),
            "sandbox" =>getenv("ZOHO_sandbox"),
            "applicationLogFilePath"=>getenv("ZOHO_applicationLogFilePath"),
            "currentUserEmail"=>getenv("ZOHO_currentUserEmail"),
            "client_id"=>getenv("ZOHO_client_id"),
            "client_secret"=>getenv("ZOHO_client_secret"),
            "redirect_uri"=>getenv("ZOHO_redirect_uri"),
            "accounts_url"=>getenv("ZOHO_accounts_url"),
            "token_persistence_path"=>getenv("ZOHO_token_persistence_path"),
            "access_type"=>getenv("ZOHO_access_type"),
            "persistence_handler_class"=>getenv("ZOHO_persistence_handler_class")                    
        ]);

        $oAuthClient = \ZohoOAuth::getClientInstance($this->_module);
        
        return $oAuthClient->generateAccessTokenFromRefreshToken(
            $this->_token,
            $this->_userIdentifier);

            
    }

    public function insertRecord(array $data)
    {
        $this->_data = $data;
        if (!is_array($this->_data)) {
            return [
                "error"=> true,
                "message"=> "Parameters should be an array"
            ];            
            
        }

        if (empty($this->_data)) {
            return [
                "error"=> true,
                "message"=> "Data not Found"
            ];                        
        }
       
        try {
            $this->_zcrmRecord = \ZCRMRecord::getInstance($this->_module, "");
            $this->setDataToSendZoho();
            $result = $this->_zcrmRecord->create();
    
            return [
                "error"=> false,
                "data"=> $result->getData()->getEntityId()
            ];                           
        } catch(\ZCRMException $e) {

            return [
                "error"=> true,
                "data"=> $e->getMessage()
            ];
        }

    }

    public function updateRecord(array $data)
    {
        $this->_data = $data;
        if (!is_array($this->_data)) {
            return [
                "error"=> true,
                "message"=> "Parameters should be an array"
            ];                        
        }

        if (empty($this->_data)) {
            return [
                "error"=> true,
                "message"=> "Data not Found"
            ];                                    
        }

        if (empty($this->_data['entityId'])) {
            return [
                "error"=> true,
                "message"=> "entityId not found"
            ];                                    
        }


        try {
            $this->_zcrmRecord = \ZCRMRecord::getInstance($this->_module, "");
            $this->setDataToSendZoho();
            
            $this->_zcrmRecord->setEntityId($this->getEntityId());
            $result = $this->_zcrmRecord->update();

            return [
                "error"=> false,
                "data"=> $result->getData()->getEntityId()
            ];                           
        } catch(\ZCRMException $e) {

            return [
                "error"=> true,
                "data"=> $e->getMessage()
            ];
        }
    }

    public function getRecordById($id)
    {
        if (empty($id)) {
            return [
                "error"=> true,
                "message"=>"entityId not found"
            ];
        }

        try {
            $apiResponse = \ZCRMModule::getInstance($this->_module)->getRecord($id);
            $record=$apiResponse->getData();
            
            return [
              "error"=> false,
              "data"=>$record->getData()
            ];
        } catch (Exception $e) {
            return [
                "error"=> true,
                "message"=>$e->getMessage()
            ];
        }
        
    }


    public function setModule($module)
    {
        $this->_module = $module;
    }

    public function setDataToSendZoho()
    {
        if (empty($this->_data)) {
            return null;
        }
            
        foreach($this->_data as $key=>$value) {
            $this->_zcrmRecord->setFieldValue($key,$value);
        }        
    }


    public function getEntityId()
    {
        return $this->_data['entityId'];
    }

    public function getFilesById($id)
    {
        if (empty($id)) {
            return [
                "error"=> true,
                "message"=>"entityId not found"
            ];
        }

        try {
            $apiResponse = \ZCRMRecord::getInstance($this->_module,$id);
            $record=$apiResponse->getAttachments();

            return [
                "error"=> false,
                "data"=>$record->getData()
            ];
        } catch (Exception $e) {
            return [
                "error"=> true,
                "message"=>$e->getMessage()
            ];
        }

    }

    public function downloadFiles($lead,$file_id)
    {
        $apiResponse = \ZCRMRecord::getInstance($this->_module,$lead);
        $fileResponseIns=$apiResponse->downloadAttachment($file_id);
        $fp=fopen('lead'.$fileResponseIns->getFileName(),"w");
        echo "HTTP Status Code:".$fileResponseIns->getHttpStatusCode();
        echo "File Name:".$fileResponseIns->getFileName();
        $stream=$fileResponseIns->getFileContent();
        fputs($fp,$stream);
        fclose($fp);

    }

    
    public function uploadAttachment($lead, $file)
    {
        if (empty($lead)) {
            return [
                "error"=> true,
                "message"=>"entityId not found"
            ];            
        }


        try {
            $record = \ZCRMRecord::getInstance($this->_module,$lead);
            $fileResponseIns = $record->uploadAttachment($file);    

            return [
                "error"=> false,
                "data"=>$fileResponseIns->getMessage()
            ];            
        }

        catch (Exception $e) {
            return [
                "error"=> true,
                "message"=>$e->getMessage()
            ];
        }
    }


    
}