<?php //proyecto/app/bootstrap.php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

//configuración para cargar las vistas
define('VIEW_PATH', APP_PATH . 'views/');
//configuración del router


$requestContext = new RequestContext();
$requestContext->fromRequest($request); // el contexto se actualiza con la info del request

$locator = new FileLocator(array(APP_PATH . 'config/')); // nos ubicamos en la carpeta app/config
$loader = new YamlFileLoader($locator); //creamos el loader

$options = array(
    'cache_dir' => APP_PATH . 'cache/', //directorio donde serán cacheadas las rutas
    'debug' => DEBUG, // depende de la constante creada en public/web.php
);

$router = new Router($loader, 'routing.yml', $options, $requestContext);

//configuración de ruteo

try {
    $match = $router->match($request->getPathInfo()); //obtenemos la uri desde el pathinfo
    $request->attributes->add($match); //agregamos los datos definidos para la ruta en el request
    //leemos el indice _file definido en la opción defaults de la ruta.
    $file = APP_PATH . 'controller/' . $match['_file'];
    
    if (!is_file($file)) {
        throw new InvalidArgumentException("Not Found the Controller File " . $file);
    }

    ob_start(); //vamos a capturar la salida para agregarla luego a un objeto response
    $response = require $file;
    /* Los controladores pueden devolver instancias de Response 
     * o imprimir su contenido Directamente.
     *
     * Si no se devuelve una instancia de response, la creamos y le pasamos el contenido del buffer
     */
    if (!($response instanceof Response)) {
        $response = new Response(ob_get_clean());
    }
} catch (ResourceNotFoundException $e) {
    if (DEBUG) {
        throw new ResourceNotFoundException("URL Not Found"
        . $request->getPathinfo(), $e->getCode(), $e); //en desarrollo mostramos la excepción
    } else {
        // en producción creamos una respuesta
        $response = new Response('Internal Server Error', 500);
    }
}

return $response; //devolvemos la respuesta