<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class ProjectUrlMatcher extends Symfony\Component\Routing\Matcher\UrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = array(
            '/' => array(array(array('_route' => 'inicio', '_file' => 'inicio.php'), null, null, null, false, false, null)),
            '/leads' => array(array(array('_route' => 'leads_list', '_file' => 'leads/list.php'), null, null, null, true, false, null)),
            '/leads/create' => array(array(array('_route' => 'leads_create', '_file' => 'leads/create.php'), null, null, null, false, false, null)),
            '/leads/edit' => array(array(array('_route' => 'leads_edit', '_file' => 'leads/update.php'), null, null, null, false, false, null)),
            '/leads/all.json' => array(array(array('_route' => 'leads_list_json', '_file' => 'leads/list.json.php'), null, null, null, false, false, null)),
        );
        $this->regexpList = array(
            0 => '{^(?'
                    .'|/leads/(?'
                        .'|getRecordById/([^/]++)(*:39)'
                        .'|show\\-files/([^/]++)(*:66)'
                        .'|([^/]++)/upload_file(*:93)'
                    .')'
                .')/?$}sD',
        );
        $this->dynamicRoutes = array(
            39 => array(array(array('_route' => 'leads_get_record_by_id', '_file' => 'leads/search.php'), array('entityId'), null, null, false, true, null)),
            66 => array(array(array('_route' => 'leads_files', '_file' => 'leads/files.php'), array('entityId'), null, null, false, true, null)),
            93 => array(array(array('_route' => 'lead_upload_file', '_file' => 'leads/uploadFile.php'), array('entityId'), null, null, false, false, null)),
        );
    }
}
