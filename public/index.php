<?php
ini_set("max_execution_time", 3600);
ini_set("memory_limit", "512M");
ini_set("display_errors", 0);
error_reporting(-1);

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

Debug::enable();
//Debug::enable(null, false); //descomentar en producción y comentar el anterior

define('APP_PATH', dirname(__DIR__) . '/app/'); //contiene la ruta hasta app
define('DEBUG', true); // true en desarollo y false en producción

$request = Request::createFromGlobals(); //creamos el objeto Request

// env variables
$dotenv = new Dotenv\Dotenv(APP_PATH."../");
$dotenv->load();

$response = require_once APP_PATH . 'bootstrap.php'; //cargamos el bootstrap

$response->send(); //enviamos la respuesta de vuelta
